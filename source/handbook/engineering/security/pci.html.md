---
layout: handbook-page-toc
title: "GitLab SOC2 Security Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is PCI?

PCI is the shorthand often used for the payment card industry's data security standard (PCI-DSS) as defined by the [PCI security standards council](https://www.pcisecuritystandards.org/). The PCI-DSS defines the requirements of businesses that accept or facilitate credit card payments based on type or amount of transactions accepted or facilitated.

## What are GitLab's requirements for PCI compliance?

Because of the low number of credit card transactions GitLab is involved with and the fact that we use a third-party payment processor, GitLab is a level 4 merchant for PCI. Level 4 merchants are required to complete an annual self attestation questionnaire (SAQ) and in particular an SAQ-A form. Quarterly scanning of our PCI systems must also be performed by an approved scanning vendor (ASV).

## What is GitLab's current state of PCI compliance?

GitLab has completed an SAQ-A form and has regularly scans of PCI systems performed by an ASV. The SAQ-A form (and associated Attestation of Compliance (AoC)) and the results of our ASV scans are available upon request. Please send an email to [security@gitlab.com](mailto:security@gitlab.com) to request these documents. 

## What is considered a PCI system?

Determining how a system may interact with the payment processing system is important when defining which systems are in-scope for PCI compliance.  The following diagram demonstrates in-scope (connected-to) vs. out-of-scope (not connected-to) when defining the PCI environment:

```mermaid
graph TB

  SubGraph2Flow
  subgraph "Out-of-Scope B"
  SubGraph2Flow(System D)
  end

  SubGraph1Flow
  subgraph "Out-of-Scope A"
  SubGraph1Flow(System C)
  end

  subgraph "In-Scope for PCI"
  Node1[Payment Processing System] --> Node2[Connected-To System A]
  Node1[Payment Processing System] --> Node3[Connected-To System B]
  Node2 --> SubGraph1Flow(System C)
  Node3 --> SubGraph2Flow(System D)
end
```
**Please Note:**
*  *Connected-to systems create a buffer for the out-of-scope systems from directly connecting to the payment processing system.*
*  *It does not matter where a system resides (i.e. different cloud environment), if it communicates with the payment processing system, it is in-scope for PCI.*

## Who can I reach out to if I have additional questions about PCI as it relates to GitLab?

The [GitLab security compliance team](/handbook/engineering/security/compliance.html#contact-the-compliance-team) can help with any questions relating to PCI.
