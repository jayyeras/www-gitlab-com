---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is account based marketing?
Account-based marketing is a strategic approach to marketing based on account awareness in which an organization considers and communicates with individual prospect or customer accounts as markets of one.  Through a close alignment between sales and marketing we focus on target accounts that fit our ICP or ideal customer profile.  At GitLab, we are at the beginning of our account based marketing efforts and in the process of defining our ICP and aligning our target accounts based on those criteria.

## Where does account based marketing fit within the greater marketing org?
Account based marketing sits next to field marketing and is similarly aligned to sales.  Account based marketing looks at an account or target account as a market of one, versus marketing to the total addressable market.  This type of marketing is executed by the accoutn based team where as field marketing etc is focused on lead gen and accoutn centric marketing.

## What is an ideal customer profile (ICP) and how do we determine what that looks like?
An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our target list of highest value accounts.  The account based marketing team is responsible for the management of the ICP and reviews of our ICP as it iterates and changes based on time, company goals, and product maturity. We have engaged TOPO to assist with this process, and you can follow along in the [ICP Epic](https://gitlab.com/groups/gitlab-com/-/epics/210)

## Roles & Responsibilities

**Emily Luehrs**  
*Account Based Marketing Manager*
* **Development**:plan account based marketing strategy, prioritze company ojectives as it aligns with ABM
* **Strategy**: plan, prioritize and manage excution of campaigns
* **Ideal Customer Profile**: acts as project manager for the development of our ICP

**Jenny Tiemann**  
*Sr. Marketing Program Manager*
* **Campaigns**: organize execution, timeline, and campaign tracking

**Leslie Stinson**  
*Digital Marketing Manager*
* **Campaigns**: develop and manage digital assets and implementation for ABM campaigns

## Tools we use

**Demandbase** 
Targeting and personalization platform which we use to target online ads to companies that fit our ICP and tiered account criteria.  

**TOPO**
Research and advisory firm used by companies to develop and orchestrate their account based strategy.  We will be following their model for developing our ideal customer profile (ICP) and account based orchestration plays.
[TOPO research we are using](https://drive.google.com/drive/folders/1PC9Fqri-_JiJM1107B7k-ejD20gV3CnM?usp=sharing)

## Account Based Marketing workflow and labels in GitLab   

The ABM team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~Account Based Marketing label anywhere within the GitLab repo. 

The ABM team uses this [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/1409957) and also has the [Account Based Marketing Project](gitlab.com/gitlab-com/marketing/account-based-marketing)

Labels used by the team:  
- `Account Based Marketing`: pulls the issue into the board, and is used to put an issue on the team's radar
- `ABM FYI`: used to put something on the account based marketing team's radar, however they are not the DRI not is any action needed at this time (example: and account centric campaign being run by field marketing)
- `ABM Campaign`: Used to identify account based marketing campaigns
- `ABM::plan`: work or campaign that has been proposed and is awaiting evaluation form the team.  This includes additional being added to our ABM strategy and campaign proposals
- `ABM::design`: work or campaign that has been accepted by the ABM team and is being flushed out before starting the execution work
- `ABM::wip`: issues that are currently being worked on by the ABM team
- `ABM::blocked`: issues/work that is currently blocked.  The team may be waiting in additional information or could be plannign to execute but at a later date due to other circumstances
- (deprecated) `ABM 30 day`, `ABM 60 day`, & `ABM 90 day`: labels used to track our initial rollout of our ABM program.  These labels are used by the internal team only, but our board is organized so that other can follow along


## Definitions
**Total addressable Market (TAM)**-
Also called total available market, total addressable market references the revenue opportunity available for a product or service. TAM helps to prioritize business opportunities by serving as a quick metric of the underlying potential of a given opportunity

**Ideal customer profile (ICP)**- 
Ideal customer profile is a description of a company who is the best fit for our solution.  This can include firmographics, environmental and behavioral characteristics.  We use this profile to align our account based marketing efforts

**Target accounts**- 
Accounts that fit our ideal customer profile that we will focus our account base strategy on.  Target accounts are simply accounts that we would like to make customers

**Tiered Accounts**- 
Our account based strategy will include tiering our target accounts based on a number of factors so that we can market appropriately to them.  We consider our ICP, intent signals, and other data when adding accounts to a tier.

## Ideal Customer Profile

### Tier 1 (1:1 or strategic) - ICP accounts
**Our highest value target accounts.  Will include 5-10 accounts at any given time globally. Will receive customized account marketing plan w personalized content**
1.  be a 100% match to our ideal customer profile + additional criteria based on our focus at the time (i.e. a certain vertical, etc)
1.  be showing high intent signals
1.  top account for sales
1.  have an engaged sales team ready to enact a fully customized marketing plan
      - have a completed marketing plan developed through collaboration of the ABM and Sales team for that account

### Tier 2 (1:few or scale)-Like accounts based on a certain number of target account qualifiers
**Will include roughly 25 accounts at any given time globally.  These accounts will receive an orchestration play that is a medium lift and focused on like accounts.  This mean there may be three different campaigns running in the segment, focused on a set of accounts rather than customized to a single account.**
1.  account fitting all but one of our ideal customer profile (ICP)
1.  must be showing medium or high intent (Demandbase)
1.  high priority for sales but may have a lower lifetime value for GitLab

### Tier 3 (1:many or programmatic)- Accounts we would like to target but without the personalize plan or resources
**Will include 75-100 accounts globally at any given time. These accounts will have a digital campaign based on use case along with light email and SDR support**
1.  fits some but not all ICP criteria AND
1.  showing medium intent signals OR
1.  is a focus for sales in the territory

## Adding an account to our ABM strategy

### Accounts are defined in Salesforce by the following GTM strategy:

**BLANK**
Default for all accounts

**VOLUME**
Indicates the accounts are in an account centric marketing plan i.e. will be included in geo-targeted marketing efforts and efforts by the field marketing team.

**ABM**
This defines that an account is included in one of the three tiers of our account based strategy.

**There is an update that is planned where the tier in our ABM strategy for the account is also defined in salesforce.  ETA is Q2 FY21**

### When does an account move tiers?
The account based marketing team will be monitoring accounts in all tiers and adjusting the level of marketing support for these accounts based on the accounts tiering qualifications.  For an example, if an account is currently in tier 3 and then meets all of tier 2 qualifications, that account will be moved to tier 2.  Additionally, the account team will be notified and brought in to strategize and engage in the marketing plan appropriately.

Accounts can also move tiers by being nominated by sales and accepted by the account based team.  This is done through completing this issue template and assigning it to @emilyluehrs.  The account will be evaluated and SLA for a decision within 5 business days of the issue being submitted.

### Other times when accounts will move
As our ICP iterates (we will be reviewing in July 2020 and then annually after that) we will be moving accounts into the account based strategy and could also be removing accounts or moving them to a lower tier.  This could be because the intent signals have dropped to a level that does not support a custom marketing plan, or the sales team is not supporting the marketing plan as needed.  An explanation will always be given if an account is moved to a different tier or removed from our account based strategy altogether.

## How we use intent data
We use intent data to help us target the right accounts at the right time.  By knowing when an account is has the highest propensity to buy, we can target accounts with the right resources at the right time.

### Intent definitions
In salesforce, you can now see a set of new fields at the account level.  Below is a list of those fields and their definitions.

**Score** Demandbase uses AI to qualify accounts and give them a score based on their match to our ICP and their engagement on our website, and also offsite intent.  Score are ranked High, Medium, and Low.

**Trending onsite engagement** Shows us which accounts have been more engaged and active on our site over the last week compared to the last two months.

**Page views** The number of uniqie page views within the last thirty days.  We have scrubbed the list of pages that count towards this number of irrelevent pages when considering purchasing GitLab (i.e. careers page).

**Sessions** Shows ths number of unique visits the account has made to our website in the last thirty days.  A session is defined as a a unique visit to our site of up to 30 minutes.  If they spend more than 30 minutes on our site this will count as two sessions.  It is generally considered that an account is engaged if they have three or more sessions in the past thirty days.

**Intent** This field will show you the top five keywords the account is researching

**Trending offsite intent** Shows if the account has had a recent spike in offsite research, including competitor research



