---
layout: markdown_page
title: Product Direction - Growth
description:
---

## Overview

### Growth Team

Product Manager: [Tim Hey](/company/team/#timhey) | 
Product Manager: [Sid Reddy](/company/team/#sid_reddy) |
Product Manager: [Jensen Stava](/company/team/#jstava) | 

### Growth Team Mission

### Growth OKRs

### Growth KPIs

### Our approach

### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 

### Problems we solve

### What user workflows does the Growth team focus on?

### Key User Focus:

#### External
The external user personas on GitLab.com and our Self-Managed instances are very different and should be treated as such. We will be focusing on the 2 described below. 

In addition to the personas, it's also important to understand the permissions available for users (limits and abilities) at each level. 
* [Handbook page on permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab)
* [Docs on permission on docs.gitlab.com](https://docs.gitlab.com/ee/user/permissions.html)
* *Note: GitLab.com and Self-Managed permission do differ.*  

#### GitLab Team Members
*  Sales Representatives
*  Customer Success Representatives
*  Support Representatives

### Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)

### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
